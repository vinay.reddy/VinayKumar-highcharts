var express = require('express');
var path = require('path');
var app = express();
var fs = require('fs');
const port = 5000;
app.use(express.static(path.join(__dirname, './webapp')));
fs.readFile('primaryschool.json', function (err, data) {
  if (err) {
    throw err;
  }
  content = JSON.parse(data);;
});
app.get("/schooldata", function (req, res) {
  res.send(schoolsPerDistrict(content));
});
app.get("/categorydata", function (req, res) {
  res.send(categoryPerDistrict(content));
});
app.get("/moidata", function (req, res) {
  res.send(mediumsPerDistrict(content));
});
app.get("/content", function (req, res) {
  res.sendFile(path.join(__dirname, 'primaryschool.json'));
});
function removeDuplicates(arr) {
  var unique_array = [];
  arr.forEach(function (element) {
    if (unique_array.indexOf(element) == -1) {
      unique_array.push(element);
    }
  });
  return unique_array;
}
function schoolsPerDistrict(content) {
 return groupBy(content, function (item) {
    return item.district_name;
  });
}
function groupBy(array, f) {
  var groups = {};
  array.forEach(function (obj) {
    var group = f(obj);
    if(group!=undefined)
    {
    groups[group] = groups[group] || [];
    groups[group].push(obj);
    }
  });
  return groups;
}
function mediumsPerDistrict(content) {
  var result = groupBy(content, function (item) {
    return item.district_name;
  });
  var uniqueDistricts = Object.keys(result);
  uniqueDistricts.forEach(function (district) {
    var moidata = groupBy(result[district], function (item) {
      if (item.moi!=="")
      return item.moi;
      else
      return;
    })
    result[district] = moidata;
  })
  return result;
}
function categoryPerDistrict(content) {
  var result = groupBy(content, function (item) {
    return item.district_name;
  });
  var uniqueDistricts = Object.keys(result);
  uniqueDistricts.forEach(function (district) {
    var catData = groupBy(result[district], function (item) {
      return item.cat;
    });
    result[district] = catData;
  });
  return result;
}

app.listen(port, function () {
  console.log('Magic happens on port ' + port);
});