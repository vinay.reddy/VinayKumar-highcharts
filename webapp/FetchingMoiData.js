fetch('/moidata')
  .then(function (response) {
    return response.json();
  })
  .then(function (myJson) {
    var uniqueDistricts=Object.keys(myJson);
    var set=new Set();
  uniqueDistricts.forEach(function (district) {
    var medium=Object.keys(myJson[district])
    medium.forEach(function(element){
    set.add(element);
  });
  });
  var mediums =Array.from(set);
  var mediumDataArr = mediums.map(function (medium) {
      return { "name": medium, "data": uniqueDistricts.map(function (dist) {
        if(myJson[dist][medium]===undefined) 
        return 0;
        else
         return myJson[dist][medium].length
        }) }
    })
    Highcharts.chart('container3', {
      chart: {
        type: 'column'
      },
      title: {
        text: 'Number of School according to medium'
      },
      xAxis: {
        categories: uniqueDistricts
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Number of schools'
        },
        stackLabels: {
          enabled: true,
          style: {
            fontWeight: 'bold',
            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
          }
        }
      },
      legend: {
        align: 'right',
        x: -30,
        verticalAlign: 'top',
        y: 25,
        floating: true,
        backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
        borderColor: '#CCC',
        borderWidth: 1,
        shadow: false
      },
      tooltip: {
        headerFormat: '<b>{point.x}</b><br/>',
        pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
      },
      plotOptions: {
        column: {
          stacking: 'normal',
          dataLabels: {
            enabled: true,
            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
          }
        }
      },
      series: mediumDataArr
    });

  });