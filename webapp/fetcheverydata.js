
$(document).ready(function () {
  fetch('/schooldata')
    .then(function (response) {
      return response.json();
    })
    .then(function (myJson) {
      var uniqueDistricts = Object.keys(myJson);
      $.each(uniqueDistricts, function (index, value) {
        $('.menu').append('<a href="#"><li class="district-name"><span>' + "+ " + value + '</span></li></a>')
      });
    });
  $(document.body).on('click', '.district-name', function (event) {
    var districtItem = $(this).children(':first-child').text();
    var districtName = $(this).text().split(' ')[1];
    if (districtItem === '+ ' + districtName) {
      $(this).children(':first-child').text('- ' + districtName);
    }
    else {
      $(this).children(':first-child').text('+ ' + districtName);
    }
    if ($(this).children().length === 1) {
      $(this).append('<div class="district-items"><ul style="padding-left: 41px;margin: 15px;"></ul></div>');
      $(this).find('ul').append('<li class="medium"><span> + Mediums</span></li>')
      $(this).find('ul').append('<li class="categories"><span>+ Categories</span></li>')
    }
    $(this).find('.district-items').toggle();
  });
  $(document.body).on('click', '.medium', function (event) {
    event.stopPropagation();
    var districtName = $(this).parents('.district-name').text().split(' ')[1];
    var mediumItem = $(thisVar).children(':first-child').text();
    if ($(this).children(':first-child').text() === ' + Mediums') {
      $(this).children(':first-child').text(' - Mediums');
    }
    else {
      $(this).children(':first-child').text(' + Mediums');
    }
    var thisVar = this;
    fetch('/moidata')
      .then(function (response) {
        return response.json();
      })
      .then(function (myJson) {
        var uniqueDistricts = Object.keys(myJson);
        var mediums = [];
        uniqueDistricts.forEach(function (district) {
          var medium = Object.keys(myJson[district])
          medium.forEach(function (element) {
            if (mediums.indexOf(element) == -1) {
              mediums.push(element);
            }
          });
        });
        medium(mediums, myJson)
      });
    function medium(mediums, myJson) {
      if ($(thisVar).children().length === 1) {
        $(thisVar).append('<div class="medium-items"><ul style="padding-left: 41px;margin: 15px;"></ul></div>');
        $.each(mediums, function (index, value) {
          if (myJson[districtName][value] === undefined) {
              $(thisVar).find('ul').append('<li>' + value + ': 0' + '</li>')
          }
          else {
              $(thisVar).find('ul').append('<li>' + value + ': ' + myJson[districtName][value].length + '</li>')
            }
        });
      };
      $(thisVar).find('.medium-items').toggle();
    }
  });




  $(document.body).on('click', '.categories', function (event) {
    event.stopPropagation();
    var categories = $(this).children(':first-child').text();
    if ($(this).children(':first-child').text() === '+ Categories') {
      $(this).children(':first-child').text('- Categories');
    }
    else {
      $(this).children(':first-child').text('+ Categories');
    }
    var districtName = $(this).parents('.district-name').text().split(' ')[1];
    var thisVar = this;
    fetch('/categorydata')
      .then(function (response) {
        return response.json();
      })
      .then(function (myJson) {
        var uniqueDistricts = Object.keys(myJson);
        var categories = [];
        uniqueDistricts.forEach(function (district) {
          var category = Object.keys(myJson[district])
          category.forEach(function (element) {
            if (categories.indexOf(element) == -1) {
              categories.push(element);
            }
          });
        });
        category(categories, myJson)
      });

    function category(categories, myJson) {
      if ($(thisVar).children().length <= 1) {
        $(thisVar).append('<div class="category-items"><ul style="padding-left: 41px;margin: 15px;"></ul></div>');
        $.each(categories, function (index, value) {
          if (myJson[districtName][value] === undefined) {
            $(thisVar).find('ul').append('<li>' + value + ': 0</li>')
          }

          else {
            $(thisVar).find('ul').append('<li>' + value + ': ' + myJson[districtName][value].length + '</li>')
          }
        });
      };
      $(thisVar).find('.category-items').toggle();
    }

  });
});