fetch('/categorydata')
  .then(function (response) {
    return response.json();
  })
  .then(function (myJson) {
var uniqueDistricts=Object.keys(myJson);
    var set=new Set();
  uniqueDistricts.forEach(function (district) {
    var category=Object.keys(myJson[district])
    category.forEach(function(element){
      set.add(element);
  });
  });
  var categories = Array.from(set);
  var catDataArr = categories.map(function (category) {
      return { "name": category, "data": uniqueDistricts.map(function (dist) {
        if(myJson[dist][category]===undefined) 
        return 0;
        else
         return myJson[dist][category].length
        }) }
    })
    Highcharts.chart('container2', {
      chart: {
          type: 'column'
      },
      title: {
          text: 'Schools per District according to cateogeory'
      },
      xAxis: {
          categories: uniqueDistricts,
          crosshair: true
      },
      yAxis: {
          min: 0,
          title: {
              text: 'Number of schools'
          }
      },
      tooltip: {
          headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
          pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
              '<td style="padding:0"><b>{point.y:.0f} schools</b></td></tr>',
          footerFormat: '</table>',
          shared: true,
          useHTML: true
      },
      plotOptions: {
          column: {
              pointPadding: 0.2,
              borderWidth: 0
          }
      },
      series: catDataArr
  });
  });