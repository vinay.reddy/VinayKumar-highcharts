fetch('/schooldata')
  .then(function(response) {
    return response.json();
  })
  .then(function(myJson) {
     var uniqueDistricts=Object.keys(myJson);
    var countArr=uniqueDistricts.map(function(district){
return myJson[district].length;
    });
  Highcharts.chart('container1', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Schools per District'
    },
    xAxis: {
        categories: uniqueDistricts,
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Number of schools'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.0f} schools</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'City',
        data:countArr

    }]
});
});
